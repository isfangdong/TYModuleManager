//
//  TYModuleMainLoginProtocol.h
//  TYModuleManager
//
//  Created by 房栋 on 2018/5/7.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol TYModuleMainLoginProtocol <NSObject>

@optional
- (Class)mainLoginViewController;

@end
