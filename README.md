# TYModuleManager

[![CI Status](https://img.shields.io/travis/isfang/TYModuleManager.svg?style=flat)](https://travis-ci.org/isfang/TYModuleManager)
[![Version](https://img.shields.io/cocoapods/v/TYModuleManager.svg?style=flat)](https://cocoapods.org/pods/TYModuleManager)
[![License](https://img.shields.io/cocoapods/l/TYModuleManager.svg?style=flat)](https://cocoapods.org/pods/TYModuleManager)
[![Platform](https://img.shields.io/cocoapods/p/TYModuleManager.svg?style=flat)](https://cocoapods.org/pods/TYModuleManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TYModuleManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TYModuleManager'
```

## License

TYModuleManager is available under the MIT license. See the LICENSE file for more info.
